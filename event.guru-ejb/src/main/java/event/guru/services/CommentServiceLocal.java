package event.guru.services;

import java.util.List;

import javax.ejb.LocalBean;

import event.guru.DTO.Comment;



@LocalBean
public interface CommentServiceLocal {
	void Create(Comment c, String id, String email);
	void Update(Comment c , int id);
	List<Comment> GetAll();
	Comment GetOne(int id);
	void Delete(Comment id);


}
