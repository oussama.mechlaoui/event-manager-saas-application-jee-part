package event.guru.services;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import event.guru.DTO.Comment;



@Stateful
@LocalBean
public class CommentService implements CommentServiceRemote {
	public String GlobalEndPoint = "empresentation-dev.eu-west-1.elasticbeanstalk.com";
	@Override
	public void Create(Comment c, String userid, String user_email) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://"+GlobalEndPoint+"/api/comments/create");
		WebTarget hello =target.path("");
		c.setParticipantId(userid);
		c.setParticipantName(user_email);
		Response response =hello.request().post(Entity.entity(c, MediaType.APPLICATION_JSON) );
		
		String result=response.readEntity(String.class);
		System.out.println(result);

		response.close();
		
		
	}
	
	@Override
	public void Update(Comment c , int id) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://"+GlobalEndPoint+"/api/comments/update="+id);
		WebTarget hello =target.path("");
		
		Response response =hello.request().put(Entity.entity(c, MediaType.APPLICATION_JSON) );
		
		String result=response.readEntity(String.class);
		System.out.println(result);

		response.close();
		
	}
	
	@Override
	public List<Comment> GetAll() {
		List<Comment>  lasp = new ArrayList<Comment>();
    	Client client = ClientBuilder.newClient();
    	
    	WebTarget web = client.target("http://"+GlobalEndPoint+"/api/comments"); 
    	
    	Response response = web.request().get();
    	
    	String result = response.readEntity(String.class); 
    	
    	JsonReader jsonReader = Json.createReader(new StringReader(result));
    	JsonArray object =  jsonReader.readArray();
    	//SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    	String format = "dd/MM/yy H:mm:ss"; 

    	java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format ); 
    	java.util.Date date = new java.util.Date(); 

    	
    	 
    	
    	for (int i=0;i<object.size();i++)
    	{
    	 
    		Comment c = new Comment();
    		
       	
       	
    	 
    	 c.setId(object.getJsonObject(i).getInt("Id")); 
    	 c.setText(object.getJsonObject(i).getString("Text")); 
    	 c.setCreatedOn(date);
    	 c.setParticipantId(object.getJsonObject(i).getString("ParticipantId"));
    	 c.setParticipantName(object.getJsonObject(i).getString("ParticipantName"));
    	 
    	 lasp.add(c);
    	}
    	

return lasp;    	
	}
	
	
	@Override
	public Comment GetOne(int id) {
		List<Comment>  lasp = new ArrayList<Comment>();
    	Client client = ClientBuilder.newClient();
    	
    	WebTarget web = client.target("http://"+GlobalEndPoint+"/api/comments"); 
    	
    	Response response = web.request().get();
    	
    	String result = response.readEntity(String.class); 
    	
    	
    	JsonReader jsonReader = Json.createReader(new StringReader(result));
    	JsonArray object =  jsonReader.readArray();
    	
    	String format = "dd/MM/yy H:mm:ss"; 

    	java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format ); 
    	java.util.Date date = new java.util.Date(); 
    	
    	 
    		Comment c = new Comment();
    		
    		
    	 c.setId(object.getJsonObject(0).getInt("Id")); 
    	 c.setText(object.getJsonObject(0).getString("Text")); 
    	 c.setCreatedOn(date);

    	
    	 
 return c;   	
	}
	
	@Override
	public void Delete(Comment c) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://"+GlobalEndPoint+"/api/comments/delete/"+c.getId());
		WebTarget hello =target.path("");
		
		Response response =hello.request().delete( );
		
		//String result=response.readEntity(String.class);
		//System.out.println(result);

		response.close();
		
	}
	
	
	

}
