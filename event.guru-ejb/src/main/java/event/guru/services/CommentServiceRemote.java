package event.guru.services;
import java.util.List;

import javax.ejb.Remote;

import event.guru.DTO.Comment;




@Remote
public interface CommentServiceRemote {
	void Create(Comment c, String id, String email);
	void Update(Comment c , int id);
	List<Comment> GetAll();
	Comment GetOne(int id);
	void Delete(Comment id);


}
