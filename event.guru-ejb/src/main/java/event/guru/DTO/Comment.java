package event.guru.DTO;


import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



	//@JsonIgnoreProperties(ignoreUnknown = true)
	public class Comment implements Serializable {
		
		//@JsonProperty("Id")
		public int Id;
		
		//@JsonProperty("Text")
		public String Text;
		
		//@JsonProperty("CreatedOn")
		public Date CreatedOn;
		
		public String ParticipantId;
		
		public String ParticipantName;

		public int getId() {
			return Id;
		}

		public void setId(int id) {
			Id = id;
		}

		public String getText() {
			return Text;
		}

		public void setText(String text) {
			Text = text;
		}

		public Date getCreatedOn() {
			return CreatedOn;
		}

		public void setCreatedOn(Date createdOn) {
			CreatedOn = createdOn;
		}
		

		public String getParticipantId() {
			return ParticipantId;
		}

		public void setParticipantId(String participantId) {
			ParticipantId = participantId;
		}

		public String getParticipantName() {
			return ParticipantName;
		}

		public void setParticipantName(String participantName) {
			ParticipantName = participantName;
		}

		public Comment() {
			super();
		}

		public Comment(int id, String text, Date createdOn) {
			super();
			Id = id;
			Text = text;
			CreatedOn = createdOn;
		}

	
		
	}
		
		