package managedBeans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import event.guru.DTO.Comment;
import event.guru.services.CommentService;



@ManagedBean
@ViewScoped
public class CommentBean {
	private int Id;
	private String Text;
	private Date CreatedOn;
	private String ParticipantId;
	private String ParticipantName;
	@ManagedProperty(value = "#{user}")
	private UserBean userBean;
	
	private List<Comment> comms;
	
	@EJB
	CommentService cs;
	
	
	@PostConstruct
	public void init(){
		System.out.println("tt"+cs.GetOne(2).toString());
		comms=cs.GetAll();
		//System.out.println(userBean.getUserId());
	
	}
	public String BackToComment(){
		return "/front-pages/index?faces-redirect=true";
	}
	
	public String DelCom(Comment c){
		cs.Delete(c);
		return "/front-pages/index?faces-redirect=true";
	}
	public String AddCom(String id, String name){
		cs.Create(new Comment(Id,Text,CreatedOn),id, name);
		return "/front-pages/index?faces-redirect=true";
	}
	public CommentBean() {
		super();
	}
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getText() {
		return Text;
	}
	public void setText(String text) {
		Text = text;
	}
	public Date getCreatedOn() {
		return CreatedOn;
	}
	public void setCreatedOn(Date createdOn) {
		CreatedOn = createdOn;
	}
	
	public String getParticipantId() {
		return ParticipantId;
	}
	public void setParticipantId(String participantId) {
		ParticipantId = participantId;
	}
	public String getParticipantName() {
		return ParticipantName;
	}
	public void setParticipantName(String participantName) {
		ParticipantName = participantName;
	}
	public List<Comment> getComms() {
		return comms;
	}
	public void setComms(List<Comment> comms) {
		this.comms = comms;
	}
	public UserBean getUserBean() {
		return userBean;
	}
	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}
	
	
	
	
	
	


}
