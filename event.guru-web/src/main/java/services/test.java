package services;

import java.io.StringReader;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.client.JerseyWebTarget;
import org.glassfish.jersey.filter.LoggingFilter;
import entities.Token;
import entities.User;

public class test {
	
	public static ArrayList<User> Pending(String token){
		String endpointURL ="http://10.0.0.1:4640/api/tenant/pending";
    	JerseyClientBuilder client = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
    	JerseyWebTarget target =  client.build().target(endpointURL); 
	    // Provide the authorization information
	    target.register((ClientRequestFilter) requestContext -> {
	    	 requestContext.getHeaders().add("Authorization", "Bearer "+token);
	    	});   
	    //Invocation.Builder invocationBuilder =  target.request();
	    Response response_user = target.request().accept(MediaType.APPLICATION_JSON).get();
	    if(response_user.getStatus() != 200) {
	    	throw new RuntimeException("Failed : HTTP error code : "+ response_user.getStatus());
	    	
	    }
	    else {
	    	String result = response_user.readEntity(String.class);
	    	JsonReader jsonReader = Json.createReader(new StringReader(result));
	    	JsonArray object =  jsonReader.readArray();
	    	ArrayList<User> users = new ArrayList();
	    	for (int i=0;i<object.size();i++)
	    	{
	    	 
	    		User u = new User();
	    	 //String dateString;
	       	 u.setUserId(object.getJsonObject(i).getString("Id"));
	       	 u.setTenantId(String.valueOf(object.getJsonObject(i).getInt("TenantId")));
	       	 u.setTenantName(object.getJsonObject(i).getString("TenantName"));
	       	 u.setUserName(object.getJsonObject(i).getString("UserName"));
	       	 u.setEmail(object.getJsonObject(i).getString("Email"));
	       	 u.setRole(object.getJsonObject(i).getString("Role"));
	       	 u.setStatus(object.getJsonObject(i).getString("Status")); 
	    	
	    	 users.add(u);
	    	}
	    	return users;
	    }       	    
	}
	
	public static void Logo() {
		Form form = new Form();
        form.param("grant_type", "password");
        form.param("username", "admin@acloudguru.com");
        form.param("password", "Azerty@12");
        JerseyClientBuilder jerseyClientBuilder = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
        JerseyWebTarget jerseyWebTarget =      jerseyClientBuilder.build().target("http://10.0.0.1:4640/oauth/token");        
        Response response = jerseyWebTarget.request().accept(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form));
        Token token = response.readEntity(Token.class);
        System.out.println(token.getAccessToken());
        //System.out.println(response);
	}
	
	public static String Login(String username, String password) {
		// TODO Auto-generated method stub
		Form form = new Form();
        form.param("grant_type", "password");
        form.param("username", username);
        form.param("password", password);
        JerseyClientBuilder jerseyClientBuilder = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
        JerseyWebTarget jerseyWebTarget =      jerseyClientBuilder.build().target("http://10.0.0.1:4640/oauth/token");        
        Response response = jerseyWebTarget.request().accept(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form));
        if (response.getStatus() == 200) {
        	Token token = response.readEntity(Token.class);
        	response.close();
        	String endpointURL ="http://10.0.0.1:4640/api/tenant/user/admin@acloudguru.com/";
        	JerseyClientBuilder client = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
        	JerseyWebTarget target =  client.build().target(endpointURL); 
    	    // Provide the authorization information
    	    target.register((ClientRequestFilter) requestContext -> {
    	    	 requestContext.getHeaders().add("Authorization", "Bearer "+token.getAccessToken());
    	    	});
    
    	    //Invocation.Builder invocationBuilder =  target.request();
    	    Response response_user = target.request().accept(MediaType.APPLICATION_JSON).get();
    	    if(response_user.getStatus() != 200) {
    	    	throw new RuntimeException("Failed : HTTP error code : "+ response_user.getStatus());
    	    }
    	    else {
    	    	User result = response_user.readEntity(User.class);
    	    	result.setToken(token.getAccessToken());
    	    	response_user.close();
    	    	return result.toString();
    	    	
    	    }       	
        	
        }
        return "access_denied";	    
	}
	
	
	public static String register() {
		Form form = new Form();
        form.param("Email", "test12322@test.com");
        form.param("Password", "Azerty@12");
        form.param("ConfirmPassword", "Azerty@12");
        form.param("TenantName", "Tenant");
        JerseyClientBuilder client = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
        JerseyWebTarget target =      client.build().target("http://10.0.0.1:4640/api/tenant/create");        
        Response response = target.request().accept(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form));
        if (response.getStatus() == 201) {
        	return "ok";
        }
        return "ko";
	}
	
	
	public static String Reject(String token, String id) {
		Form form = new Form();
		String endpointURL ="http://10.0.0.1:4640/api/tenant/reject/"+id;
    	JerseyClientBuilder client = new JerseyClientBuilder().register(new LoggingFilter(java.util.logging.Logger.getAnonymousLogger(), true));
    	JerseyWebTarget target =  client.build().target(endpointURL); 
	    // Provide the authorization information
	    target.register((ClientRequestFilter) requestContext -> {
	    	 requestContext.getHeaders().add("Authorization", "Bearer "+token);
	    	});

	    //Invocation.Builder invocationBuilder =  target.request();
	    Response response_user = target.request().accept(MediaType.APPLICATION_JSON).post(Entity.form(form));
	    if(response_user.getStatus() != 200) {
	    	return "ok";
	    }
	    else {
	    	return "ko";	    	
	    }
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String tok = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1laWQiOiIwNTEwOTk0My03NjJhLTQ3MzItYTc4MC00ZmZmMzEyNzIwMTQiLCJ1bmlxdWVfbmFtZSI6ImFkbWluQGFjbG91ZGd1cnUuY29tIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS9hY2Nlc3Njb250cm9sc2VydmljZS8yMDEwLzA3L2NsYWltcy9pZGVudGl0eXByb3ZpZGVyIjoiQVNQLk5FVCBJZGVudGl0eSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiZjgyNmI4MWYtYmU4MC00ZWZiLWFhZTgtYWQyMjk5Mjc3OGJjIiwicm9sZSI6IkFkbWluaXN0cmF0b3IiLCJBY3RpdmUiOiJhY3RpdmVkIiwiaXNzIjoiaHR0cDovLzE5Mi4xNjguMS4zMDo0NjQwIiwiYXVkIjoiMDk5MTUzYzI2MjUxNDliYzhlY2IzZTg1ZTAzZjAwMjIiLCJleHAiOjE1NTc0NzgxNDcsIm5iZiI6MTU1NzM5MTc0N30.SbxU6T9tXoZZKhE88hpZN4NOLTGkZ04P624gynG_jlU";
		String id = "48699bfc-4f2f-4332-b12e-c99909cc3ec9";
		//System.out.println(Login(username,password));
		System.out.println(Reject(tok, id));
		//GetALL();
	}

}
